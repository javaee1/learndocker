# LearnDocker

## Welcome Docker Commands
```sh
$ docker version # verified cli can talk to engine
$ docker info # most config values of engine
$ docker --help # self explanatory
```

## Image vs Container
- An image is the application we want to run.
- A Container is an instance of that image running as a process.
- You can have many container running off the same image.
- Docker's default image "registry" is called Docker Hub(hub.docker.com).

## HighLevel view of Docker Architecture

![Docker Architecute](.doc/docker-architecture.jpg) 

**`Note:`&nbsp;** Docker has client-server architecture.

## Components of Docker

- Docker Client
- Docker Daemon
- Docker Images
- Dockerfile
- Docker Registries
- Containers
- Docker Engine


**`Docker Client: `** This is the CLI (Command Line Interface) tool used to configure and interact with Docker. eg: `docker run`

**`Docker Daemon: `** This is the Docker server which runs as the daemon. Process the docker command.

**`Docker Images: `** Images are the read-only template/snapshot used to create a Docker container and these Images can be pushed to and pulled from public or private repositories.

**`Dockerfile: `** Used for building images.

**`Docker Registries: `** Central repository of Docker images which stores Docker images. 

**`Containers: `** Running instance of image (process).

**`Docker Engine: `** Combination of Docker daemon, Rest API and CLI tool.

## Container Specific Commands
```sh
$ docker container run --publish 80:80 nginx
$ docker container run --publish 8080:80 --detach nginx
$ docker container ls # list active container
$ docker container stop ($id)
$ docker container ls -a # list all container
$ docker container run --publish 8080:80 --detach --name webhost nginx
$ docker container logs webhost
$ docker container top webhost
$ docker container --help
$ docker container rm  ($id) # delete container (only if it stop)
$ docker container rm -f ($id) # delete container (force)
$ docker container prune # delete all stoped container.
$ docker container top # process list in one container.
$ docker container inspect # details of one container config.
$ docker container stats - performance stats
```

## Inspect Command
```sh
$ docker image inspect
$ docker container inspect
$ docker network inspect
```

## Volume Management

**Volume Releated commands**

```sh
 $ docker volume create $(volname)
 $ docker volume inspect $(volname)
 $ docker volume rm $(volname)
 $ docker volume ls 
 $ docker volume prune
```

## Docker Networking

![Docker Communication](.doc/DockerkNetworkArchitecture.png)